package com.sgr.test.techtest.api;

import android.os.AsyncTask;

/**
 * Created by shekh on 22/3/18.
 * <p>
 * Handles Asynchronous task for networking call
 * This class only deals with GET Request
 * POST/PUT/DELETE has been skipped as it was not required for this task
 */

public class NetworkTask extends AsyncTask<Void, Void, String> {

    private NetworkListener mListener;
    private String mUrl;
    private String mRequestType;

    NetworkTask(NetworkListener listener, String url, String requestType) {
        mListener = listener;
        mUrl = url;
        mRequestType = requestType;
    }

    @Override
    protected String doInBackground(Void... voids) {
        String responseStr = "";
        switch (mRequestType) {
            case NetworkCall.GET:
                responseStr = NetworkCall.get(mUrl);
                break;
            default:
                break;
        }
        return responseStr;
    }

    @Override
    protected void onPostExecute(String responseStr) {
        super.onPostExecute(responseStr);
        mListener.postExecution(responseStr);
    }

    public interface NetworkListener {
        void postExecution(String responseStr);
    }
}


