package com.sgr.test.techtest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.sgr.test.techtest.api.ApiProcessor
import com.sgr.test.techtest.model.ResponseObj
import com.sgr.test.techtest.model.RowItem
import com.sgr.test.techtest.ui.RowAdapter
import com.sgr.test.techtest.util.AppInfo
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    private var mRowItemList = ArrayList<RowItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        when {
            savedInstanceState == null -> callApi()
            savedInstanceState.getBoolean(AppInfo.BUNDLE_KEY_IS_REFRESHING) -> {
                refreshUiWithBundle(savedInstanceState)
                callApi()
            }
            else -> refreshUiWithBundle(savedInstanceState)
        }

        fab.setOnClickListener {
            callApi()
        }

        swipe_refresh_layout.setOnRefreshListener {
            callApi()
        }
    }

    /**
     * refresh UI with Bundle data
     *
     * @param savedInstanceState
     */
    private fun refreshUiWithBundle(savedInstanceState: Bundle?) {
        savedInstanceState?.getParcelableArrayList<RowItem>(AppInfo.BUNDLE_KEY_SAVED_LIST).let {
            it?.let {
                mRowItemList = it
                refreshList(mRowItemList)
            }
        }

        savedInstanceState?.getString(AppInfo.BUNDLE_KEY_TITLE)?.let {
            refreshActionBar(it)
        }
    }

    /**
     * Calls API for server request
     */
    private fun callApi() {
        swipe_refresh_layout.isRefreshing = true
        ApiProcessor.getInstance().getList(object : ApiProcessor.ProcessorListener {
            override fun resultSuccess(responseObj: ResponseObj?) {
                swipe_refresh_layout.isRefreshing = false
                Log.d("RESPONSE_OBJ", responseObj?.title + responseObj?.rows?.count())
                refreshUi(responseObj)
            }

            override fun resultFailed() {
                swipe_refresh_layout.isRefreshing = false
            }
        })
    }

    /**
     * Refresh Whole UI with response object's value
     *
     * @param responseObj value to refresh UI with
     */
    private fun refreshUi(responseObj: ResponseObj?) {
        responseObj?.let {
            refreshActionBar(it.title)
            it.rows?.let {
                mRowItemList = it
                refreshList(mRowItemList)
            }
        }
    }

    /**
     * Sets Action bars title
     *
     * @param title title of the actionbar
     */
    private fun refreshActionBar(title: String) {
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = title
    }

    /**
     * Refresh recycler view with list of RowItem
     *
     * @param list RowItem List
     */
    private fun refreshList(list: ArrayList<RowItem>) {
        recycler_view.layoutManager = LinearLayoutManager(this)
        val rowAdapter = RowAdapter(this@MainActivity, list)
        recycler_view.adapter = rowAdapter
        rowAdapter.notifyDataSetChanged()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(AppInfo.BUNDLE_KEY_SAVED_LIST, mRowItemList)
        outState?.putString(AppInfo.BUNDLE_KEY_TITLE, supportActionBar?.title?.toString())
        outState?.putBoolean(AppInfo.BUNDLE_KEY_IS_REFRESHING, swipe_refresh_layout.isRefreshing)
    }
}