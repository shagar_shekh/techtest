package com.sgr.test.techtest.util;

/**
 * Created by shekh on 22/3/18.
 */

public class AppInfo {
    public static final String URL = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json";

    public static final String BUNDLE_KEY_SAVED_LIST = "SAVED_LIST";
    public static final String BUNDLE_KEY_TITLE = "APP_TITLE";
    public static final String BUNDLE_KEY_IS_REFRESHING = "IS_REFRESHING";
}
