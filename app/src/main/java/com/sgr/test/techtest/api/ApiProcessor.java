package com.sgr.test.techtest.api;

import com.google.gson.GsonBuilder;
import com.sgr.test.techtest.model.ResponseObj;
import com.sgr.test.techtest.util.AppInfo;

/**
 * Created by shekh on 22/3/18.
 * <p>
 * API to places the request and parse response
 */

public class ApiProcessor {
    private static ApiProcessor instance;

    private ApiProcessor() {
    }

    public static ApiProcessor getInstance() {
        if (instance == null) {
            instance = new ApiProcessor();
        }
        return instance;
    }

    private ProcessorListener mProcessorListener;

    public void getList(ProcessorListener processorListener) {
        mProcessorListener = processorListener;
        new NetworkTask(new NetworkTask.NetworkListener() {
            @Override
            public void postExecution(String responseStr) {
                ResponseObj responseObj = null;
                if (responseStr != null && !responseStr.isEmpty()) {
                    responseObj = new GsonBuilder().create().fromJson(responseStr, ResponseObj.class);
                }
                if (responseObj != null) {
                    mProcessorListener.resultSuccess(responseObj);
                } else {
                    mProcessorListener.resultFailed();
                }
            }
        }, AppInfo.URL, NetworkCall.GET).execute();
    }

    public interface ProcessorListener {
        void resultSuccess(ResponseObj responseObj);

        void resultFailed();
    }
}