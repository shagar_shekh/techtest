package com.sgr.test.techtest.model;

import java.util.ArrayList;

/**
 * Created by shekh on 22/3/18.
 * <p>
 * Model class for response object
 */

public class ResponseObj {
    private String title;
    private ArrayList<RowItem> rows;

    public ResponseObj() {
    }

    public ResponseObj(String title, ArrayList<RowItem> rows) {
        this.title = title;
        this.rows = rows;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<RowItem> getRows() {
        return rows;
    }

    public void setRows(ArrayList<RowItem> rows) {
        this.rows = rows;
    }
}
