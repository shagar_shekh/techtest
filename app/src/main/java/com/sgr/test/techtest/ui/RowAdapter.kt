package com.sgr.test.techtest.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.sgr.test.techtest.R
import com.sgr.test.techtest.model.RowItem

/**
 * Created by shekh on 22/3/18.
 *
 * Adapter to show RowItem
 */
class RowAdapter(context: Context, list: ArrayList<RowItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mContext = context
    private val mListItems: ArrayList<RowItem> = list

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RowViewHolder(LayoutInflater.from(mContext).inflate(R.layout.adapter_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val rowItem = mListItems[position]
        val rowHolder = holder as RowViewHolder
        rowItem.title?.let {
            rowHolder.itemTitle.text = it
        }

        rowItem.imageHref?.let {
            Glide.with(mContext)
                    .load(it)
                    .apply(RequestOptions().placeholder(R.drawable.ic_broken_image_black_48dp).centerCrop())
                    .transition(DrawableTransitionOptions().crossFade())
                    .into(rowHolder.itemImage)
        }

        rowItem.description?.let {
            rowHolder.itemDescription.text = it
        }
    }

    override fun getItemCount(): Int {
        return mListItems.size
    }

    private class RowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemTitle: TextView = itemView.findViewById(R.id.list_item_title_tv)
        val itemImage: ImageView = itemView.findViewById(R.id.list_item_image_iv)
        val itemDescription: TextView = itemView.findViewById(R.id.list_item_description_tv)
    }
}