package com.sgr.test.techtest.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by shekh on 22/3/18.
 * <p>
 * Using OkHttp place request to the server
 * This class only deals with GET Request
 * POST/PUT/DELETE has been skipped as it was not required for this task
 */

public class NetworkCall {
    public static final String GET = "GET";

    /**
     * Sends GET Request
     *
     * @param url URL
     * @return response String
     */
    public static String get(String url) {
        Request request = new Request.Builder().url(url).method("GET", null).build();
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        String responseStr = "";
        try {
            Response response = client.newCall(request).execute();
            if (response != null && response.body() != null) {
                responseStr = response.body().string();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseStr;
    }
}
